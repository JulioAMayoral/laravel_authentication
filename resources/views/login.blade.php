<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="output.css">
    <title>Document</title>
    @vite('resources/css/app.css')
</head>

<body class="bg-blue-400">

    <form action="{{route('inicia_sesion')}}" method="POST" class="bg-white w-80 mx-auto mt-8 rounded-lg p-6">
        @csrf

        <input
            class="border border-gray-300 w-full px-3 py-2 rounded-md focus:outline-none focus:ring-1 invalid:focus:ring-red-400 peer"
            type="email" placeholder="Correo" name="email">
        <p class="text-red-400 hidden peer-invalid:block">El correo es incorrecto</p>
        <input class="border border-gray-300 w-full px-3 mt-4 py-2 mb-4 rounded-md" type="password"
            placeholder="Contraseña" name="password">
        <input type="checkbox" name="remember">
        <label for="rememberCheck" class="">Mantener sesión iniciada</label>
        <input class="bg-blue-500 mt-4 w-full py-2 text-white rounded-md cursor-pointer hover:bg-blue-400" type="submit"
            value="Ingresar">
            <div class="w-full px-3 py-2">
                ¿No tienes cuenta?<a class="text-blue-500" href="{{route('register')}}"> Registrate</a>
            </div>
    </form>

</body>

</html>
