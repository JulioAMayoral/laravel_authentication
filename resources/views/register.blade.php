<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite('resources/css/app.css')
</head>
<body class="bg-gray-500">
    <form action="{{route('validar-registro')}}" method="POST" class="bg-white w-80 mx-auto mt-8 rounded-lg p-6">
        @csrf

        <input class="border border-gray-300 w-full px-3 py-2 mb-4 rounded-md disabled:bg-red-200" type="text"
            placeholder="Nombre" name="name">

        <input
            class="border border-gray-300 w-full px-3 py-2 rounded-md focus:outline-none focus:ring-1 invalid:focus:ring-red-400 peer"
            type="email" placeholder="Correo" name="email">
        <p class="text-red-400 hidden peer-invalid:block">El correo es incorrecto</p>

        <input class="border border-gray-300 w-full px-3 mt-4 py-2 mb-4 rounded-md" type="password"
            placeholder="Contraseña" name="password">

        <input class="bg-blue-500 w-full py-2 text-white rounded-md cursor-pointer hover:bg-blue-400" type="submit"
            value="Ingresar">

    </form>
</body>
</html>
