<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite('resources/css/app.css')
</head>
<body>
    <section>
        <p class="text-lg text-center p-4">Contenido privado
            @auth
            de {{Auth::user()->name}}
            @endauth
        </>
        <div>
            <a href="{{route('logout')}}">
                <button class="p-2 bg-blue-300 hover:bg-azul-oscuro rounded-md w-24 mx-auto block my-8" type="button" class="">
                    Salir
                </button>
            </a>
        </div>
    </section>
</body>
</html>
