<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

Route::view("/login", "login")->name("login");
Route::view("/registro", "register")->name("register");
Route::view("/privada", "secret")->middleware('auth')->name("privada");

Route::post("/validar-registro", [LoginController::class,'register'])->name("validar-registro");
Route::post("/iniciar_sesion", [LoginController::class,'login'])->name("inicia_sesion");

Route::get("/logout", [LoginController::class,'logout'])->name("logout");



Route::get('/', function () {
    return view('welcome');
});
